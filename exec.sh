#!/bin/bash

set -xe

IMG_BASE=$1
IMG_FINAL=$2
OFFSET=$3
BOARD=$4

# Global constants
MOUTED_IMG_FOLDER="internal/mounted-img"
NEW_IMG_FOLDER="internal/mount-temp"
TMP_FOLDERS="$MOUTED_IMG_FOLDER $NEW_IMG_FOLDER"
CONTROLLAR_FOLDER="$MOUTED_IMG_FOLDER/controllar"
UPLOADS="internal/uploads"
COMMANDS="$UPLOADS/commands"

mkdir -p $TMP_FOLDERS

touch public/generated-images/loading-$IMG_FINAL.gz
cp internal/base-images/$IMG_BASE $NEW_IMG_FOLDER/$IMG_FINAL
mount -t ext4 -o rw,sync,offset=$OFFSET $NEW_IMG_FOLDER/$IMG_FINAL $MOUTED_IMG_FOLDER
cp $UPLOADS/*.db3 $UPLOADS/*.ovpn $CONTROLLAR_FOLDER

if [ -e "$UPLOADS/nsPanel.json" ] ; then
cp $UPLOADS/nsPanel.json  $CONTROLLAR_FOLDER
fi

if [ -e "$COMMANDS.zip" ] ; then
unzip $COMMANDS.zip -d $UPLOADS/
chmod +x $COMMANDS
mv -f $COMMANDS $CONTROLLAR_FOLDER
rm $COMMANDS.zip
fi
cd $CONTROLLAR_FOLDER

expect << EOF
spawn ./maleta.sh
expect "Digite 1 para configurar o WebServer. Ou digite 2 para configurar a ShowCase:\r"
send -- "1\r"
expect "Digite 1 para Laranja, 2 para Raspberry Pi2 e 3 para Raspberry Pi3:\r"
send -- "$BOARD\r"
expect "Digite 1 para Sistema Novo e 2 para Sistema Velho:\r"
send -- "1\r"
expect "Sim/Nao\r"
send -- "Sim\r"
expect eof
EOF
cd -

# unmount before copy to avoid inconsistent copy
umount $MOUTED_IMG_FOLDER

gzip $NEW_IMG_FOLDER/$IMG_FINAL
cp $NEW_IMG_FOLDER/$IMG_FINAL.gz public/generated-images/$IMG_FINAL.gz

# Cleanup environment
bash clean.sh

echo "--Exec Done--"
